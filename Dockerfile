FROM node:8.12-alpine

WORKDIR /app

ADD . /app
COPY package.json /app
RUN cd /app && npm install

CMD ["node", "server.js"]

EXPOSE 3000