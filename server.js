const express = require('express')
const pathToSwaggerUi = require('swagger-ui-dist').absolutePath()

const app = express()

app.use(express.static(__dirname))
app.use(express.static(pathToSwaggerUi))

listenPort = process.env.PORT || 4000,

app.listen(listenPort)
