# documentation

Steps to create docker package:

```
docker build . -t documentation 
docker run -p 4000:4000 -e PORT=4000 -it documentation
```

Steps to push package:

```
docker login
docker tag documentation rrobledo/documentation
docker push rrobledo/documentation
```
